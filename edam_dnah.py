import sys
from tkinter import *
from tkinter import filedialog as fd
from tkinter import messagebox as mb
from datetime import datetime
import pandas as pd
import numpy as np

# ===================================================================================================
# функция очистки строки от ' (число)' в конце
def cleaned_substring(substring):
    last_space_index = substring.rfind(' ')
    if last_space_index != -1 and substring[last_space_index + 1] == '(' and substring[-1] == ')':
        try:
            int(substring[last_space_index + 2:-1])
            cleaned_substring = substring[:last_space_index]
        except ValueError:
            cleaned_substring = substring
    else:
        cleaned_substring = substring
    return cleaned_substring
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# функция обработки нажатия кнопки Сформировать на стартовом окне
def convert2save():
    Text_1 = "{}".format(txt1.get())
    Text_2 = "{}".format(txt2.get())
    Text_22 = "{}".format(txt22.get())
    Text_3 = "{}".format(txt3.get())
    Text_4 = "{}".format(txt4.get())
    Text_5 = "{}".format(txt5.get())
    Text_6 = "{}".format(txt6.get())    
    #================================================
    #Выбор выходного файла для импорта
    file_name_out_mm =''
    while file_name_out_mm =='':
        file_name_out_mm = fd.asksaveasfilename(title='Выбирите файл для импорта в ОМ',defaultextension=".xlsx",filetypes=[("Excel файл", ".xlsx")],initialfile="Файл для импорта в ОМ от " + str(datetime.now().date()) + ' ' + str(datetime.now().hour) + '∶' + (str(datetime.now().minute) if datetime.now().minute>9 else '0'+str(datetime.now().minute)))
    #================================================
    start_time = datetime.now()
    #================================================
    with pd.ExcelFile(file_name_Prop) as excel_file:
        prop = pd.read_excel(excel_file, sheet_name='Лист1')
        prop_prop = {}
        for index, row in prop.iterrows():
            prop_prop[row['Name']] = row['ValueTypePath'] + '#' + row['DataTypePath']
    #================================================
    with pd.ExcelFile(file_name_Pr2) as excel_file:
        Pr2 = pd.read_excel(excel_file, sheet_name='ОМ ручного ввода',skiprows=1)
        Pr2 = Pr2.replace(np.nan, '', regex=True) # замена всех NaN на пустую строку
        Pr2_Pr2 = Pr2.to_dict(orient='records')
    #================================================
    dt_L1 = {}
    # извлечение данных из файла приложения 2
    for row_i in range(0,len(Pr2_Pr2)):

        # извлечение значений столбцов из исходной таблицы по каждой строке
        # так как невозможно предсказать формат ячейки, то все конвертируем в строку
        #------- UNIT ---------
        UnitName_Unit_tmp = str(Pr2_Pr2[row_i]['Наименование раздела [UnitName]'])
        UnitOrder_Unit_tmp = str(Pr2_Pr2[row_i]['Порядковый номер [UnitOrder]'])
        IsNaturalEntry_Unit_tmp = str(Pr2_Pr2[row_i]['Ввод по требованию [IsNaturalEntry]'])
        TimeZone_Unit_tmp = str(Pr2_Pr2[row_i]['Временная зона [TimeZone]'])
        #------- SHEET --------
        SubUnit_Sheet_tmp = str(Pr2_Pr2[row_i]['Группа листов [SubUnit]'])
        SheetName_Sheet_tmp = str(Pr2_Pr2[row_i]['Наименование листа [SheetName]'])
        SheetOrder_Sheet_tmp = str(Pr2_Pr2[row_i]['Порядковый номер [SheetOrder]'])
        Duration_Sheet_tmp = str(Pr2_Pr2[row_i]['Тип листа [Duration]'])
        InputExactTime_Sheet_tmp = str(Pr2_Pr2[row_i]['Ввод на метку времени листа [InputExactTime]'])
        EnterPeriods_Sheet_tmp = str(Pr2_Pr2[row_i]['Время ввода листа [EnterPeriods]'])
        InputEventTypeCode_Sheet_tmp = str(Pr2_Pr2[row_i]['Событие оповещения о необходимости заполнения [InputEventTypeCode]'])
        #------- PARAM --------
        SubUnit_Param_tmp = str(Pr2_Pr2[row_i]['Группа параметров [SubUnit]'])
        ParameterName_Param_tmp = str(Pr2_Pr2[row_i]['Наименование параметра'])
        ParameterShortName_Param_tmp = str(Pr2_Pr2[row_i]['Короткое наименование параметра\n[ShortName]'])
        ParamOrder_Param_tmp = str(Pr2_Pr2[row_i]['Порядковый номер [ParamOrder]'])
        IsRequired_Param_tmp = str(Pr2_Pr2[row_i]['Обязательный параметр [IsRequired]'])
        EnterPeriods_Param_tmp = str(Pr2_Pr2[row_i]['Время ввода параметра [EnterPeriods]'])
        EnterBefore_Param_tmp = str(Pr2_Pr2[row_i]['Нижняя граница ввода по расписанию в минутах [EnterBefore]'])
        EnterAfter_Param_tmp = str(Pr2_Pr2[row_i]['Верхняя граница ввода по расписанию в минутах [EnterAfter]'])
        IsReadOnly_Param_tmp = str(Pr2_Pr2[row_i]['Только для чтения [IsReadOnly]'])
        InputMask_Param_tmp = str(Pr2_Pr2[row_i]['Маска ввода [InputMask]'])
        EngUnits_Param_tmp = str(Pr2_Pr2[row_i]['Единицы измерения [EngUnits]'])
        Tag_Param_tmp = str(Pr2_Pr2[row_i]['Имя тега [Tag]'])
        #----------------
        # создание Unit и заполнение его свойствами
        UnitName_Unit_tmp = UnitName_Unit_tmp.strip()

        if dt_L1.get(UnitName_Unit_tmp) is None:
            row_tmp={}

            # наименование раздела
            row_tmp['UnitName']=UnitName_Unit_tmp

            # порядковый номер раздела
            row_tmp['UnitOrder'] = UnitOrder_Unit_tmp.split('.')[0] if UnitOrder_Unit_tmp.split('.')[0].isdigit() else '1'
                
            # ввод по требованию
            row_tmp['IsNaturalEntry'] = 'false' if IsNaturalEntry_Unit_tmp.strip().lower() != 'да' else 'true'
                
            # временная зона
            row_tmp['TimeZone'] = TimeZone_Unit_tmp.replace(' ','') if TimeZone_Unit_tmp not in ['',' ','-','_','None','Russian Standard Time'] else 'UTC+03:00'
                
            # словарь для принадлежащих разделу листов
            row_tmp['Sheet'] = {}
                
            #----------------
            dt_L1[UnitName_Unit_tmp]=row_tmp
            #----------------

        # создание Sheet и заполнение его свойствами
        # по требованию заказчика имя листа формируется как сумма имени листа и порядкового номера в скобках
        SheetName_Sheet_tmp_old = SheetName_Sheet_tmp.strip()
        SheetName_Sheet_tmp = SheetName_Sheet_tmp.strip() + ' (' + (SheetOrder_Sheet_tmp.split('.')[0] if SheetOrder_Sheet_tmp.split('.')[0].isdigit() else '1') +')'

        if dt_L1[UnitName_Unit_tmp]['Sheet'].get(SheetName_Sheet_tmp) is None:
            row_tmp={}
                    
            # группа листов
            row_tmp['SubUnit']=SubUnit_Sheet_tmp if SubUnit_Sheet_tmp not in ['',' ','-','_','None'] else ''

            # наименование листа
            row_tmp['SheetName']=SheetName_Sheet_tmp_old #SheetName_Sheet_tmp

            # порядковый номер в разделе
            row_tmp['SheetOrder'] = SheetOrder_Sheet_tmp.split('.')[0] if SheetOrder_Sheet_tmp.split('.')[0].isdigit() else '1'

            # тип листа
            row_tmp['Duration'] = Duration_Sheet_tmp.split('.')[0] if Duration_Sheet_tmp.split('.')[0].isdigit() else '1'

            #row_tmp['Offset']='0'

            # ввод на метку времени листа 
            row_tmp['InputExactTime'] = 'false' if InputExactTime_Sheet_tmp.strip().lower() != 'да' else 'true'

            # время ввода листа
            row_tmp['EnterPeriods']=EnterPeriods_Sheet_tmp if EnterPeriods_Sheet_tmp not in ['',' ','-','_','None'] else '*_*_*'

            # событие оповещения о необходимости заполнения
            row_tmp['InputEventTypeCode']=InputEventTypeCode_Sheet_tmp if InputEventTypeCode_Sheet_tmp not in ['',' ','-','_','None'] else ''

            # словарь принадлежащих листу параметров
            row_tmp['Parameter'] = {}
                    
            #----------------
            dt_L1[UnitName_Unit_tmp]['Sheet'][SheetName_Sheet_tmp] = row_tmp
            #----------------

        # создание Parameter и заполнение его свойствами
        # по требованию заказчика имя параметра формируется как сумма имени листа и порядкового номера в скобках
        ParameterName_Param_tmp = ParameterName_Param_tmp.strip() + ' (' + (ParamOrder_Param_tmp.split('.')[0] if ParamOrder_Param_tmp.split('.')[0].isdigit() else '1') +')'

        if dt_L1[UnitName_Unit_tmp]['Sheet'][SheetName_Sheet_tmp]['Parameter'].get(ParameterName_Param_tmp) is None:
            row_tmp={}
                    
            # группа параметров
            row_tmp['SubUnit'] = SubUnit_Param_tmp if SubUnit_Param_tmp not in ['',' ','-','_','None'] else ''

            # наименование параметра
            row_tmp['ParameterName']=ParameterName_Param_tmp

            # короткое наименование параметра
            row_tmp['ShortName']=ParameterShortName_Param_tmp
            
            # порядковый номер в листе
            row_tmp['ParamOrder'] = ParamOrder_Param_tmp.split('.')[0] if ParamOrder_Param_tmp.split('.')[0].isdigit() else '1'

            # обязательный параметр
            row_tmp['IsRequired'] = 'true' if IsRequired_Param_tmp.strip().lower() != 'нет' else 'false'

            # время ввода параметра
            row_tmp['EnterPeriods']=EnterPeriods_Param_tmp if EnterPeriods_Param_tmp not in ['',' ','-','_','None'] else '*_*_*'

            # нижняя граница ввода по расписанию в минутах
            row_tmp['EnterBefore'] = EnterBefore_Param_tmp.split('.')[0] if EnterBefore_Param_tmp.split('.')[0].isdigit() else '10'

            # верхняя граница ввода по расписанию в минутах
            row_tmp['EnterAfter'] = EnterAfter_Param_tmp.split('.')[0] if EnterAfter_Param_tmp.split('.')[0].isdigit() else '60'

            # только для чтения
            row_tmp['IsReadOnly'] = 'true' if IsReadOnly_Param_tmp.strip().lower() != 'нет' else 'false'

            # маска ввода
            row_tmp['InputMask'] = InputMask_Param_tmp if InputMask_Param_tmp not in ['',' ','-','_','None'] else ''
                        
            # единицы измерения
            row_tmp['EngUnits'] = EngUnits_Param_tmp if EngUnits_Param_tmp not in ['',' ','-','_','None'] else ''

            # имя тега
            row_tmp['Tag'] = Tag_Param_tmp if Tag_Param_tmp not in ['',' ','-','_','None']else ''

            # определение класса параметра и запрет на недопустимые для данного класса свойства
            if EnterBefore_Param_tmp != '' or EnterAfter_Param_tmp!='' or EnterPeriods_Param_tmp !='':
                if InputMask_Param_tmp !='':
                    row_tmp['ParamMask'] = Text_6 #ScheduleWithMask
                    row_tmp['RmvProperty'] = ['ParameterName','ParamMask','RmvProperty'] # список недопустимых свойств параметра
                else:
                    row_tmp['ParamMask'] = Text_5 #Schedule
                    row_tmp['RmvProperty'] = ['ParameterName','ParamMask','RmvProperty','InputMask'] # список недопустимых свойств параметра
            else:
                if InputMask_Param_tmp !='':
                    row_tmp['ParamMask'] = Text_4 #RequirementWithMask
                    row_tmp['RmvProperty'] = ['ParameterName','ParamMask','RmvProperty','EnterPeriods','EnterBefore','EnterAfter'] # список недопустимых свойств параметра
                else:
                    row_tmp['ParamMask'] = Text_3 #Requirement
                    row_tmp['RmvProperty'] = ['ParameterName','ParamMask','RmvProperty','EnterPeriods','EnterBefore','EnterAfter','InputMask'] # список недопустимых свойств параметра
                        
            #----------------
            dt_L1[UnitName_Unit_tmp]['Sheet'][SheetName_Sheet_tmp]['Parameter'][ParameterName_Param_tmp] = row_tmp
            #----------------
    #================================================================================================================================================
    # обнаружение имен листов (без цифры на конце), которые встречаются только один раз в конкретном Unit
    dic_unique_sheets = {}
    # обнаружение имен параметров (без цифры на конце), которые встречаются только один раз в конкретном Unit и конкретном Sheet
    dic_unique_parameters = {}

    for row_1 in dt_L1:

        # ------------------
        dic_unique_parameters_sheet = {}
        for row_2 in dt_L1[row_1]['Sheet']:
            list_parameters = list(dt_L1[row_1]['Sheet'][row_2]['Parameter'].keys())
            
            # очистка наименований от цифры в скобках на конце
            cleaned_list_parameters = []
            for p in list_parameters:
                cleaned_list_parameters.append(cleaned_substring(p))

            # Словарь для подсчета вхождений каждого элемента
            count_dict_p = {}
            for item_p in cleaned_list_parameters:
                count_dict_p[item_p] = count_dict_p.get(item_p, 0) + 1
            # Создание нового списка с элементами, которые встречаются только один раз
            unique_list_parameters = [item_p for item_p in cleaned_list_parameters if count_dict_p[item_p] == 1]

            # словарь по производствам с именами листов встречающихся один раз в конкретном Sheet
            dic_unique_parameters_sheet[row_2] = unique_list_parameters
        # словарь по производствам с именами листов встречающихся один раз в конкретном Unit
        dic_unique_parameters[row_1] = dic_unique_parameters_sheet

        # ------------------
        list_sheets = list(dt_L1[row_1]['Sheet'].keys())
        # очистка наименований от цифры в скобках на конце
        cleaned_list_sheets = []
        for s in list_sheets:
            cleaned_list_sheets.append(cleaned_substring(s))

        # Словарь для подсчета вхождений каждого элемента
        count_dict_s = {}
        for item_s in cleaned_list_sheets:
            count_dict_s[item_s] = count_dict_s.get(item_s, 0) + 1
        # Создание нового списка с элементами, которые встречаются только один раз
        unique_list_sheets = [item_s for item_s in cleaned_list_sheets if count_dict_s[item_s] == 1]

        # словарь по производствам с именами листов встречающихся один раз в конкретном Unit
        dic_unique_sheets[row_1] = unique_list_sheets

    #================================================================================================================================================
    # инициализация заголовков целевой таблицы
    head_list = ['Id','Delete','Type','Path','Description','Code','PrototypePath',
                'PropertyPrimitivePath','UomPath','ValueTypePath','DataTypePath',
                'DataReference','Configuration','ConfigurationAutoGenerated',
                'SecurityAttributeValue','HierarchyScopePaths','PropertyType',
                'ClassPath','DescriptionOverridden']
    #================================================
    # заполнение данными целевой таблицы
    out_list =[]

    for row_1 in dt_L1:
        row_tmp={}
        #------- UNIT ----------
        row_tmp['Type'] = 'Object'
        row_tmp['Path'] = 'DataInputModel\\DataInput\\' + dt_L1[row_1]['UnitName']
        row_tmp['PrototypePath'] = 'DataInput model prototype\\DataInput\\Unit'
        row_tmp['ClassPath'] = Text_1 # по факту один класс
        out_list.append(row_tmp)
        #----- UNIT PROPER -----
        for row_2 in dt_L1[row_1]:
            if row_2 != 'Sheet':

                row_tmp={}
                row_tmp['Type'] = 'Property'
                row_tmp['Path'] = 'DataInputModel\\DataInput\\' + dt_L1[row_1]['UnitName'] + '|' + row_2
                row_tmp['PropertyPrimitivePath'] = 'DataInput model prototype|' + row_2
                row_tmp['UomPath'] = 'Не определено\\Не определено'
                row_tmp['ValueTypePath'] = prop_prop[row_2].split('#')[0]
                row_tmp['DataTypePath'] = prop_prop[row_2].split('#')[1]
                out_list.append(row_tmp)

                row_tmp={}
                row_tmp['Type'] = 'PropertyConfiguration'
                row_tmp['Path'] = 'DataInputModel\\DataInput\\' + dt_L1[row_1]['UnitName'] + '|' + row_2
                row_tmp['DataReference'] = 'Constant'
                row_tmp['Configuration'] = '{"const":"' + dt_L1[row_1][row_2] + '"}'
                out_list.append(row_tmp)

        for row_2 in dt_L1[row_1]['Sheet']:
            #----- SHEET -----
            cleaned__substring = cleaned_substring(row_2)
            if cleaned__substring in dic_unique_sheets[row_1]:
                row_2_Uniq = cleaned__substring
            else:
                row_2_Uniq = row_2

            row_tmp={}
            row_tmp['Type'] = 'Object'
            row_tmp['Path'] = 'DataInputModel\\DataInput\\' + dt_L1[row_1]['UnitName'] + '\\' + row_2_Uniq
            #row_tmp['Path'] = 'DataInputModel\\DataInput\\' + dt_L1[row_1]['UnitName'] + '\\' + row_2

            row_tmp['PrototypePath'] = 'DataInput model prototype\\DataInput\\Unit\\Sheet'
            
            #row_tmp['ClassPath'] = Text_2 # по факту один класс - НЕВЕРНО!!!

            # по факту один из двух !!! классов
            if dt_L1[row_1]['Sheet'][row_2]['Duration'] in ['1','2','3']:
                row_tmp['ClassPath'] = Text_22
            elif dt_L1[row_1]['Sheet'][row_2]['Duration'] == '4':
                row_tmp['ClassPath'] = Text_2
            else:
                row_tmp['ClassPath'] = ''
            
            out_list.append(row_tmp)

            #----- SHEET PROPER -----
            for row_3 in dt_L1[row_1]['Sheet'][row_2]:
                if row_3 != 'Parameter':
                    row_tmp={}
                    row_tmp['Type'] = 'Property'
                    row_tmp['Path'] = 'DataInputModel\\DataInput\\' + dt_L1[row_1]['UnitName'] + '\\' + row_2_Uniq +'|' + row_3
                    row_tmp['PropertyPrimitivePath'] = 'DataInput model prototype|' + row_3
                    row_tmp['UomPath'] = 'Не определено\\Не определено'
                    row_tmp['ValueTypePath'] = prop_prop[row_3].split('#')[0]
                    row_tmp['DataTypePath'] = prop_prop[row_3].split('#')[1]
                    out_list.append(row_tmp)

                    row_tmp={}
                    row_tmp['Type'] = 'PropertyConfiguration'
                    row_tmp['Path'] = 'DataInputModel\\DataInput\\' + dt_L1[row_1]['UnitName'] + '\\' + row_2_Uniq + '|' + row_3
                    row_tmp['DataReference'] = 'Constant'

                    if row_3 == 'SheetName':
                        #row_tmp['Configuration'] = '{"const":"' + row_2_Uniq + '"}'
                        row_tmp['Configuration'] = '{"const":"' + dt_L1[row_1]['Sheet'][row_2]['SheetName'] + '"}'
                    else:
                        row_tmp['Configuration'] = '{"const":"' + dt_L1[row_1]['Sheet'][row_2][row_3] + '"}'


                    out_list.append(row_tmp)

            for row_3 in dt_L1[row_1]['Sheet'][row_2]['Parameter']:
                #----- PARAM -----
                cleaned__substring = cleaned_substring(row_3)
                if cleaned__substring in dic_unique_parameters[row_1][row_2]:
                    row_3_Uniq = cleaned__substring
                else:
                    row_3_Uniq = row_3

                row_tmp={}
                row_tmp['Type'] = 'Object'
                row_tmp['Path'] = 'DataInputModel\\DataInput\\' + dt_L1[row_1]['UnitName'] + '\\' + row_2_Uniq + '\\' + row_3_Uniq
                row_tmp['PrototypePath'] = 'DataInput model prototype\\DataInput\\Unit\\Sheet\\Parameter'
                row_tmp['ClassPath'] = dt_L1[row_1]['Sheet'][row_2]['Parameter'][row_3]['ParamMask'] # по факту один из четырех !!! классов
                out_list.append(row_tmp)

                #----- PARAM PROPER -----
                for row_4 in dt_L1[row_1]['Sheet'][row_2]['Parameter'][row_3]:

                    if row_4 not in dt_L1[row_1]['Sheet'][row_2]['Parameter'][row_3]['RmvProperty']: # вывод только допустимых свойств в соответствии с определенным ранее классом
                        row_tmp={}
                        row_tmp['Type'] = 'Property'
                        row_tmp['Path'] = 'DataInputModel\\DataInput\\' + dt_L1[row_1]['UnitName'] + '\\' + row_2_Uniq + '\\' + row_3_Uniq +'|' + row_4
                        row_tmp['PropertyPrimitivePath'] = 'DataInput model prototype|' + row_4
                        row_tmp['UomPath'] = 'Не определено\\Не определено'
                        row_tmp['ValueTypePath'] = prop_prop[row_4].split('#')[0]
                        row_tmp['DataTypePath'] = prop_prop[row_4].split('#')[1]
                        out_list.append(row_tmp)

                        row_tmp={}
                        row_tmp['Type'] = 'PropertyConfiguration'
                        row_tmp['Path'] = 'DataInputModel\\DataInput\\' + dt_L1[row_1]['UnitName'] + '\\' + row_2_Uniq + '\\' + row_3_Uniq + '|' + row_4

                        if row_4 != 'Tag':
                            row_tmp['DataReference'] = 'Constant'
                            if row_4 == 'ParameterName':
                                row_tmp['Configuration'] = '{"const":"' + row_3_Uniq + '"}'
                            else:
                                row_tmp['Configuration'] = '{"const":"' + dt_L1[row_1]['Sheet'][row_2]['Parameter'][row_3][row_4] + '"}'
                        else:
                            row_tmp['DataReference'] = 'Tag'
                            row_tmp['Configuration'] = '{"tagId":"' + dt_L1[row_1]['Sheet'][row_2]['Parameter'][row_3][row_4] + '"}'
                        out_list.append(row_tmp)
    #================================================
    # сохранение результатов в Excel таблицу для импорта
    df = pd.DataFrame(out_list,columns=head_list)

    with pd.ExcelWriter(file_name_out_mm) as writer:
        df.style.set_properties(**{'border': '1px solid black', 'color': 'black', 'text-align': 'center'}).to_excel(writer, sheet_name='Models',index=False, startrow=0)
    #================================================           
    mb.showinfo('Информация','Процесс сохранения результатов завершен.\nНа обработку было потрачено ' + str(datetime.now() - start_time) + '\nДо новых встреч!')
    window.after(50,lambda:window.destroy())

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# функция обработки нажатия кнопки Тестировать на стартовом окне
def test():
    #================================================
    start_time = datetime.now()
    #================================================
    with pd.ExcelFile(file_name_Prop) as excel_file:
        prop = pd.read_excel(excel_file, sheet_name='Лист1')
        prop_prop = {}
        for index, row in prop.iterrows():
            prop_prop[row['Name']] = row['ValueTypePath'] + '#' + row['DataTypePath']
    #================================================
    with pd.ExcelFile(file_name_Pr2) as excel_file:
        Pr2 = pd.read_excel(excel_file, sheet_name='ОМ ручного ввода',skiprows=1)
        Pr2 = Pr2.replace(np.nan, '', regex=True) # замена всех NaN на пустую строку
        Pr2_Pr2 = Pr2.to_dict(orient='records')
    #================================================
    dt_L1 = {}
    dt_double_parametr = []
 
    sheet_double_number_report = ''
    parameter_double_number_report = ''

    # извлечение данных из файла приложения 2
    for row_i in range(0,len(Pr2_Pr2)):

        # извлечение значений столбцов из исходной таблицы по каждой строке
        # так как невозможно предсказать формат ячейки, то все конвертируем в строку
        #------- UNIT ---------
        UnitName_Unit_tmp = str(Pr2_Pr2[row_i]['Наименование раздела [UnitName]'])
        UnitOrder_Unit_tmp = str(int(Pr2_Pr2[row_i]['Порядковый номер [UnitOrder]']))

        #------- SHEET --------
        SubUnit_Sheet_tmp = str(Pr2_Pr2[row_i]['Группа листов [SubUnit]'])
        SheetName_Sheet_tmp = str(Pr2_Pr2[row_i]['Наименование листа [SheetName]'])
        SO = Pr2_Pr2[row_i]['Порядковый номер [SheetOrder]']
        SheetOrder_Sheet_tmp = str(int(SO) if (isinstance(SO,float) or isinstance(SO,int)) else SO)

        #------- PARAM --------
        SubUnit_Param_tmp = str(Pr2_Pr2[row_i]['Группа параметров [SubUnit]'])
        ParameterName_Param_tmp = str(Pr2_Pr2[row_i]['Наименование параметра'])
        PO = Pr2_Pr2[row_i]['Порядковый номер [ParamOrder]']
        ParamOrder_Param_tmp = str(int(PO) if (isinstance(PO,float) or isinstance(PO,int)) else PO)

        #----------------
        # создание Unit и заполнение его свойствами
        UnitName_Unit_tmp = UnitName_Unit_tmp.strip()
        #----------------
        # проверка чтобы лист с одинаковым именем (имя листа + порядковый номер) не входил в разные группы (иначе все упадет в одну группу и в один лист)
        if dt_L1.get(UnitName_Unit_tmp) is not None:
            if 'Sheet' in dt_L1[UnitName_Unit_tmp]: # если это лист - есть ключ Sheet
                for sheet_item in dt_L1[UnitName_Unit_tmp]['Sheet']: # просматриваем все листы
                    if dt_L1[UnitName_Unit_tmp]['Sheet'][sheet_item]['SubUnit'] != SubUnit_Sheet_tmp: # если группа листов отличается от текущей
                        
                        if SheetName_Sheet_tmp.strip() + ' (' + SheetOrder_Sheet_tmp +')' == dt_L1[UnitName_Unit_tmp]['Sheet'][sheet_item]['SheetName']: # и составное имя листа такое же, то значит есть повтор одного и того же имени листа в разных группах
                            sheet_double_number_report = sheet_double_number_report + 'в \"' + UnitName_Unit_tmp + '\" повтор листа: \"' + SheetName_Sheet_tmp.strip() + ' (' + SheetOrder_Sheet_tmp +')' + '\" в разных группах\n'

        #----------------
        if dt_L1.get(UnitName_Unit_tmp) is None:
            row_tmp={}

            # наименование раздела
            row_tmp['UnitName']=UnitName_Unit_tmp

            # порядковый номер раздела
            row_tmp['UnitOrder'] = UnitOrder_Unit_tmp.split('.')[0] if UnitOrder_Unit_tmp.split('.')[0].isdigit() else '1'
            
            # словарь для принадлежащих разделу листов
            row_tmp['Sheet'] = {}               

            #----------------
            dt_L1[UnitName_Unit_tmp]=row_tmp
            #----------------

        # создание Sheet и заполнение его свойствами
        # по требованию заказчика имя листа формируется как сумма имени листа и порядкового номера в скобках
        SheetName_Sheet_tmp = SheetName_Sheet_tmp.strip() + ' (' + (SheetOrder_Sheet_tmp.split('.')[0] if SheetOrder_Sheet_tmp.split('.')[0].isdigit() else '1') +')'
        #----------------
        # проверка на повтор номера параметра в листе
        if dt_L1[UnitName_Unit_tmp]['Sheet'].get(SheetName_Sheet_tmp) is not None:
            if 'Parameter' in dt_L1[UnitName_Unit_tmp]['Sheet'][SheetName_Sheet_tmp]:
                for parameter_item in dt_L1[UnitName_Unit_tmp]['Sheet'][SheetName_Sheet_tmp]['Parameter']:
                    if ParamOrder_Param_tmp in dt_L1[UnitName_Unit_tmp]['Sheet'][SheetName_Sheet_tmp]['Parameter'][parameter_item]['ParamOrder']:
                        parameter_double_number_report = parameter_double_number_report + 'в \"' + UnitName_Unit_tmp + '/' + SheetName_Sheet_tmp + '\" повтор номера параметра: ' + ParamOrder_Param_tmp + '\n'
        #----------------
        if dt_L1[UnitName_Unit_tmp]['Sheet'].get(SheetName_Sheet_tmp) is None:
            row_tmp={}
                    
            # группа листов
            row_tmp['SubUnit']=SubUnit_Sheet_tmp if SubUnit_Sheet_tmp not in ['',' ','-','_','None'] else ''

            # наименование листа
            row_tmp['SheetName']=SheetName_Sheet_tmp

            # порядковый номер в разделе
            row_tmp['SheetOrder'] = SheetOrder_Sheet_tmp.split('.')[0] if SheetOrder_Sheet_tmp.split('.')[0].isdigit() else '1'

            # словарь принадлежащих листу параметров
            row_tmp['Parameter'] = {}
                    
            #----------------
            dt_L1[UnitName_Unit_tmp]['Sheet'][SheetName_Sheet_tmp] = row_tmp
            #----------------

        # создание Parameter и заполнение его свойствами
        # по требованию заказчика имя параметра формируется как сумма имени листа и порядкового номера в скобках
        if dt_L1[UnitName_Unit_tmp]['Sheet'][SheetName_Sheet_tmp]['Parameter'].get(ParameterName_Param_tmp.strip()) is None:
            ParameterName_Param_tmp = ParameterName_Param_tmp.strip()
        else:
            ParameterName_Param_tmp = ParameterName_Param_tmp.strip() + ' (' + (ParamOrder_Param_tmp.split('.')[0] if ParamOrder_Param_tmp.split('.')[0].isdigit() else '1') +')'

        if dt_L1[UnitName_Unit_tmp]['Sheet'][SheetName_Sheet_tmp]['Parameter'].get(ParameterName_Param_tmp) is None:
            row_tmp={}
                    
            # группа параметров
            row_tmp['SubUnit'] = SubUnit_Param_tmp if SubUnit_Param_tmp not in ['',' ','-','_','None'] else ''

            # наименование параметра
            row_tmp['ParameterName']=ParameterName_Param_tmp
                        
            # порядковый номер в листе
            row_tmp['ParamOrder'] = ParamOrder_Param_tmp.split('.')[0] if ParamOrder_Param_tmp.split('.')[0].isdigit() else '1'

            #----------------
            dt_L1[UnitName_Unit_tmp]['Sheet'][SheetName_Sheet_tmp]['Parameter'][ParameterName_Param_tmp] = row_tmp
            #----------------
        else:
            dt_double_parametr.append(UnitName_Unit_tmp + ' / ' + SheetName_Sheet_tmp + ' / ' + ParameterName_Param_tmp + ' <- строка ' + str(row_i + 3))
    #================================================
    # оповещение и сохранение результатов

    if len(dt_double_parametr) > 0:
        with open("double_parameter_names.txt", "w") as file:
            for item in dt_double_parametr:
                file.write(item + "\n")
        mb.showinfo('ОШИБКА!','Были обнаружены повторы имен в ОМ!\nПовторов имен параметров: ' + str(len(dt_double_parametr)) + 
            ' шт.\nНа обработку было потрачено ' + str(datetime.now() - start_time) +'\nВнесите исправления и повторите попытку.')
    else:
        if sheet_double_number_report != '' and parameter_double_number_report != '':
            with open("double_sheet_numbers.txt", "w") as file:
                file.write(sheet_double_number_report + "\n")
            with open("double_parameter_numbers.txt", "w") as file:
                file.write(parameter_double_number_report + "\n")
            mb.showinfo('ПРЕДУПРЕЖДЕНИЕ!','Тестирование не обнаружило повторов в именах параметров, но были обнаружены повторы в порядковых номерах листов или параметров.\nНа обработку было потрачено ' + str(datetime.now() - start_time) +
                '\nВнесите исправления и повторите попытку.')
        else:
            mb.showinfo('Информация!','Тестирование завершено без ошибок!\nНа обработку было потрачено ' + str(datetime.now() - start_time) +
                '\nМожно запускать процесс формирования файла импорта')

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# функция смены режима работы программы - Тестирование или Формирование
def on_checkbutton_change ():
    btn["text"] = 'Протестировать' if var.get() else 'Сформировать'

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# функция запуска выбранного режима программы
def selector ():
    if var.get():
        test()
    else:
        convert2save()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Выбор файла Приложения 2
file_name_Pr2 = fd.askopenfilename(title='Выберите файл Приложения 2',filetypes=[("XLSX файл", ".xlsx")],initialfile="Требования к приложениям ФД v1.9.1 Ручной ввод")
if file_name_Pr2 =='':
    sys.exit()
#================================================
# Выбор файла с описанием свойств
file_name_Prop = fd.askopenfilename(title='Выберите файл с описанием свойств',filetypes=[("XLSX файл", ".xlsx")],initialfile="Type_Property")
if file_name_Prop =='':
    sys.exit()
#================================================
# создание стартовой формы ввода
window = Tk()
window.title('ЛРВ.Пр. 2 -> Импорт в ОМ')
window.geometry("710x510")
window.minsize(710, 510)
window.maxsize(710, 510)

bold_font = ("TkDefaultFont", 9, "bold")

lbl = Label(window, text="")
lbl.grid(column=0, row=1)
#-------------------------------
lbl = Label(window, text="ClassPath для Unit", font=bold_font)
lbl.grid(column=1, row=1)

txt1 = Entry(window,width=115,justify='center')
txt1.insert(-1, 'DataInput model prototype\DataInputUnit')
txt1.grid(column=1, row=2)
#-------------------------------
lbl = Label(window, text="")
lbl.grid(column=1, row=3)
lbl = Label(window, text="ClassPath для Sheet  (по запросу)", font=bold_font)
lbl.grid(column=1, row=4)

txt2 = Entry(window,width=115,justify='center')
txt2.insert(-1, 'DataInput model prototype\DataInputSheet\DataInputSheetRequirement')
txt2.grid(column=1, row=5)
#-------------------------------
lbl = Label(window, text="")
lbl.grid(column=1, row=6)
lbl = Label(window, text="ClassPath для Sheet (по расписанию)", font=bold_font)
lbl.grid(column=1, row=7)

txt22 = Entry(window,width=115,justify='center')
txt22.insert(-1, 'DataInput model prototype\DataInputSheet\DataInputSheetSchedule')
txt22.grid(column=1, row=8)
#-------------------------------
lbl = Label(window, text="")
lbl.grid(column=1, row=9)
lbl = Label(window, text="ClassPath для ParameterRequirement (по запросу)", font=bold_font)
lbl.grid(column=1, row=10)

txt3 = Entry(window,width=115,justify='center')
txt3.insert(-1, 'DataInput model prototype\DataInputParameter\DataInputParameterRequirement')
txt3.grid(column=1, row=11)
#-------------------------------
lbl = Label(window, text="")
lbl.grid(column=1, row=12)
lbl = Label(window, text="ClassPath для ParameterRequirementWithMask (по запросу с маской)", font=bold_font)
lbl.grid(column=1, row=13)

txt4 = Entry(window,width=115,justify='center')
txt4.insert(-1, 'DataInput model prototype\DataInputParameter\DataInputParameterRequirement\DataInputParameterRequirementWithMask')
txt4.grid(column=1, row=14)
#-------------------------------
lbl = Label(window, text="")
lbl.grid(column=1, row=15)
lbl = Label(window, text="ClassPath для ParameterSchedule (по расписанию)", font=bold_font)         
lbl.grid(column=1, row=16)

txt5 = Entry(window,width=115,justify='center')
txt5.insert(-1, 'DataInput model prototype\DataInputParameter\DataInputParameterSchedule')
txt5.grid(column=1, row=17)
#-------------------------------
lbl = Label(window, text="")
lbl.grid(column=1, row=18)
lbl = Label(window, text="ClassPath для ParameterScheduleWithMask (по расписанию с маской)", font=bold_font)
lbl.grid(column=1, row=19)

txt6 = Entry(window,width=115,justify='center')
txt6.insert(-1, 'DataInput model prototype\DataInputParameter\DataInputParameterSchedule\DataInputParameterScheduleWithMask')
txt6.grid(column=1, row=20)
#-------------------------------
lbl = Label(window, text="")
lbl.grid(column=1, row=21)

var = BooleanVar()
var.set(1)
chkbtn = Checkbutton(window, text="Выполнить предварительную проверку на дубли параметров в одном листе",variable=var,onvalue=True, offvalue=False,command=on_checkbutton_change)
chkbtn.grid(column=1, row=22)
#-------------------------------
lbl = Label(window, text="")
lbl.grid(column=1, row=23)

btn = Button(window, text="Протестировать", command=selector)
btn.grid(column=1, row=24)
txt1.focus()
window.mainloop()
